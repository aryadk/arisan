<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;
use Illuminate\Support\Facades\Session;
class anggotaController extends Controller
{
    function index(){
        if(Session::get('random')){
            $random = Session::get('random');
            $data = Anggota::get();
            $data1 = Anggota::where('status_bayar','belum bayar')->get();
            $belum_bayar = count($data1);
            $no = 1;
            return view('home',compact('data','no','random','belum_bayar'));
        }else{
            $data = Anggota::get();
            $data1 = Anggota::where('status_bayar','belum bayar')->get();
            $belum_bayar = count($data1);
            $no = 1;
            return view('home',compact('data','no','belum_bayar'));
        }
    }
    function tambah(Request $request){
        $data1 = new Anggota;
        $data1->nama = $request->nama;
        $data1->alamat = $request->alamat;
        $data1->status_bayar = 'belum bayar';
        $data1->status_menang = 'belum menang';
        $data1->save();

        return redirect('');
    }
    function edit(Request $request, $id){
        $data1 = Anggota::find($id);
        $data1->nama = $request->nama;
        $data1->alamat = $request->alamat;
        $data1->save();

        return redirect('');
    }
    function bayar($id){
        $update = Anggota::find($id);
        $update->status_bayar = 'sudah bayar';
        $update->save();

        return redirect('');
    }
    function kocok(){
        $data = Anggota::first();
        $data1 = Anggota::orderBy('id','desc')->first();
        $random = rand($data->id,$data1->id);
        Session::put('random',$random);
        $update = Anggota::find($random);
        $update->status_menang = 'sudah menang';
        $update->save();
        
        return redirect('');
    }
    function reset(){
        Session::flush();
        return redirect('');
    }
    function delete($id){
        $data1 = Anggota::find($id);
        $data1->delete();

        return redirect('');
    }
}
