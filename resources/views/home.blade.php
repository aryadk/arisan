<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('assets/css/bootstrap.min.css')}}">
    <script src="{{url('assets/css/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/css/jquery.js')}}"></script>

    <style>
    body{
        padding-top:2%;
    }
    </style>
</head>
<body>
    <div class="container">
    <h1>Aplikasi Arisan</h1><br> 
        <div class="row" id="content">
            <div class="col-sm-2">
                <a href="{{url('tambahV')}}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>Tambah</a> <br><br>
                @if($belum_bayar == 0)
                <a href="{{url('kocok')}}" class="btn btn-primary">Kocok Arisan</a> <br><br>
                @else
                <h5 style="color: red">Masih ada yang belum bayar, pengocokan tidak bisa dilakukan</h5><br>
                @endif
                <a href="{{url('reset')}}" class="btn btn-warning">Reset</a>
            </div>
            <div class="col-sm-10">
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Anggota</th>
                            <th>Alamat</th>
                            <th>Status Bayar</th>
                            <th>Status Menang</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                            @if(Session::get('random'))
                            @foreach($data as $data)
                                @if($data->id == $random)
                                <tr style="background-color: red">
                                @else
                                <tr>
                                @endif
                                    <td>{{$no++}}</td>
                                    <td>{{$data->nama}}</td>
                                    <td>{{$data->alamat}}</td>
                                    <td>
                                    @if($data->status_bayar=='belum bayar')
                                    <a href="{{url('bayar/'.$data->id)}}" class="btn btn-danger">Bayar</a>
                                    @else
                                    {{$data->status_bayar}}
                                    @endif
                                    </td>
                                    <td>{{$data->status_menang}}</td>
                                    <td>
                                    <a href="{{url('editV/'.$data->id)}}" class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                                    @if($data->status_bayar == 'sudah bayar')
                                    <a  href="{{url('delete/'.$data->id)}}" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                    @else
                                    <b>Bayar dulu</b>
                                    @endif
                                    </td>
                                </tr>
                            @endforeach
                                @else
                            @foreach($data as $data)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$data->nama}}</td>
                                    <td>{{$data->alamat}}</td>
                                    <td>
                                    @if($data->status_bayar=='belum bayar')
                                    <a href="{{url('bayar/'.$data->id)}}" class="btn btn-danger">Bayar</a>
                                    @else
                                    {{$data->status_bayar}}
                                    @endif
                                    </td>
                                    <td>{{$data->status_menang}}</td>
                                    <td>
                                    <a href="{{url('editV/'.$data->id)}}" class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                                    @if($data->status_bayar == 'sudah bayar')
                                    <a  href="{{url('delete/'.$data->id)}}" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                                    @else
                                    <b>Bayar dulu</b>
                                    @endif
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>