<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('assets/css/bootstrap.min.css')}}">
    <script src="{{url('assets/css/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/css/jquery.js')}}"></script>
</head>
<body>
    <div class="container">
    <h1>Form Edit Anggota</h1>
        <form action="{{url('edit/'.$id)}}" method="post">
        @csrf
            <div class="form-group">
                <label class="control-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="control-label">Alamat</label>
                <input type="text" name="alamat" class="form-control" required>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success">
            </div>
        </form>
    </div>
</body>
</html>