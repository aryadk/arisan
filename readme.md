Cara install:
1. Pastikan windows firewall dan antivirus mati.
2. clone project dengan cara buka cmd->arahkan ke htdocs-> ketik  'git clone https://gitlab.com/aryadk/arisan.git' tanpa tanda petik
4. buka cmd lagi, arahkan ke folder arisan, lalu ketik 'composer install' tanpa tanda petik di cmd
5. buka cmd klik 'php artisan migrate' tanpa tanda petik
6. saat pertama kali install data dalam database masih kosong, anda bisa menambahkannya melalui fitur tambah atau lewat dbms jika akan diuji
7. untuk membukanya bisa menggunakan browser seperti google chrome dengan cara: ketik 'php artisan serve' tanpa tanda petik di cmd
8. lalu buka di url dengan alamat localhost:8000
9. atau buka di url dengan alamat localhost/arisan/public

100% JUJUR.  waktu pengerjaan tepat 3 JAM.