<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'anggotaController@index');
Route::get('/tambahV', function(){
    return view('form_tambah');
});
Route::get('/editV/{id}', function($id){
    return view('form_edit',compact('id'));
});
Route::get('/kocok', 'anggotaController@kocok');
Route::get('/delete/{id}', 'anggotaController@delete');
Route::get('/bayar/{id}', 'anggotaController@bayar');
Route::get('/reset', 'anggotaController@reset');
Route::post('/edit/{id}','anggotaController@edit' );
Route::post('/tambah', 'anggotaController@tambah');
